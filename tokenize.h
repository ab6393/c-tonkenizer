//tokenize header file. creates the tokenize file and defines the structs and methods used
//CS243 Transition Matrix Project

// @author Andrew Baumher

#ifndef TOKENIZE_H
#define TOKENIZE_H

//included libraries
#include <stdio.h>

//a struct for transition matrix. it stores the start, end , and actual matrix, along with
//the number of rows in the matrix.
typedef struct TransitionMatrix {
	char startState;
	char endState;
	char ** matrix;
	int rows;
} TransitionMatrix;

//Takes a file that is used to define a particular Transition Matrix and creates one from it.
TransitionMatrix * createTM(FILE * fp);

//responsible for creating the actual matrix. it is a helper function for the createTM method.
//it takes the part that defines the transitions between states and turns them into a 2D array.
//the array is passed by reference, so it does not return it. 
void createMatrix(char ** matrix, int rows, FILE * fp);

//responsible for printing the 2D matrix with the Transition Matrix.
void printMatrix(int rows, char ** matrix);

//takes a token from the input and parses it to see if it is accepted or rejected, according
//to the transition matrix which is passed to it.
void parseTokens(char * tok, TransitionMatrix * tm);

//takes a character and returns what "class" it is associated with, according to the classes.h
//file provided. it is a helper function for the parseTokens method. 
int parseChar(char token);


#endif
