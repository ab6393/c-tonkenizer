//tokenize Class. implements the "classes" defined classes.h
//also uses the tokenize.h header file. 
//CS-243 DFA/Transition Matrix Project

// @author Andrew Baumher
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "classes.h"
#include "tokenize.h"

TransitionMatrix * createTM(FILE * fp)
{
	//this will take the first three lines of the file, which define the start state, end state
	//and the number of states in the matrix. these are guarenteed to be in the given file, but
	//i have done exit conditions anyway. 
	char * thatWay, * getLine, buffer[256];
	TransitionMatrix * newTM = malloc(sizeof(TransitionMatrix));
	if ( ! feof(fp))
	{
		//this is guarenteed to be in the file, but an exit is given anyway.
		if ( (getLine = fgets(buffer, 256, fp)) != NULL)
		{
			//gets the number of rows from the file and sets it in the struct
			thatWay = strtok(getLine, " ");
			thatWay = strtok(NULL, " ");
			newTM->rows = (int) strtol(thatWay, NULL, 10);
		}
		else return NULL;
	}
	else return NULL;//fail state
	
	if ( ! feof(fp))
	{
		//this is guarenteed to be in the file, but an exit is given anyway.
		if ( (getLine = fgets(buffer, 256, fp)) != NULL)
		{
			//gets the start state from the file and sets it in the struct
			thatWay = strtok(getLine, " ");
			thatWay = strtok(NULL, " ");
			newTM->startState = (int) strtol(thatWay, NULL, 10);
		}
		else return NULL;
	}
	else return NULL;//fail state

	if ( ! feof(fp))
	{
		//this is guarenteed to be in the file, but an exit is given anyway.
		if ( (getLine = fgets(buffer, 256, fp)) != NULL)
		{
			//gets the end state from the file and sets it in the struct
			thatWay = strtok(getLine, " ");
			thatWay = strtok(NULL, " ");
			newTM->endState = (int) strtol(thatWay, NULL, 10);
		}
		else return NULL;
	}
	else return NULL;//fail state
	
	newTM->matrix = (char **)malloc(newTM->rows * sizeof(char *));
	for (int w = 0; w < newTM->rows; w++)
	{
		newTM->matrix[w] = (char *)malloc(N_CC * sizeof(char));
	}
	createMatrix(newTM->matrix,newTM->rows,fp);
	
	return newTM;
}

void createMatrix(char ** matrix, int rows, FILE * fp)
{
	
	char * thatWay, * getLine, buffer[256];
	//initialize the matrix with values of -99, or a null transition. this is the default
	//transiton for all states unless defined in the file as otherwise. 
	for (int x = 0; x < rows; x++)
	{
		for (unsigned int y = 0; y < N_CC; y++)
		{
			matrix[x][y] = -99;
		}
	}
	//read the file that defines the matrix and alter any values that are not the default.
	while (!feof(fp))
	{		
		//get first string of characters. the limit for a string length is 256. 
		if ( (getLine = fgets(buffer, 256, fp)) != NULL)
		{
			thatWay = strtok(getLine, " ");
			int rowValue = strtol(thatWay, NULL, 10);
			thatWay = strtok(NULL, " /");
			while (thatWay != NULL)
			{
				//convert a token into two tokens. this is possible because these tokens follow
				//a format where the state it will transfer to and the condition that will make
				//that transition occur are separated by a '/' character. 
				//gets the column from the characters before a /. 
				int colNum = strtol(thatWay,NULL, 10);
				char * tempVal= strtok(NULL, " / \n");
				//learns where it transtitions to by the characters after the slash.
				int colVal = strtol(tempVal, NULL, 10);
				//note that 0 cannot be negated to a different value. therefore, to
				//differentiate between 0 and negation of 0, we need to substitute a new value
				//in place of 0. we are guarenteed that our max number of states is 75, so 80 
				//can be used in place of 0. see below.
				if (colVal == 0) colVal = 80;
				//in order to differentiate between a delete and a save state, while making the
				//code as efficient a possible, a character was used instead of a string. this
				//also means it is easier to parse later on. the negation of a number was used
				//to signify a delete; not so to signify a save.
				if (tempVal[strlen(tempVal)-1]== 'd')
				{
						colVal *=-1;
				}
				matrix[rowValue][colNum] = colVal;
				
				thatWay = strtok(NULL, " /");
			}
		}
	}
}
//Note that, while the project allowed us to write the print method however we best saw fit,
//i ended up implementing my code to match the sample output, which is what the submission
//was looking for. the original implementation has been left in via comments, which can be seen
//below. this method prints the given transition matrix. 
void printMatrix(int rows, char ** matrix)
{
	int columns = strlen(matrix[0]);
	printf("Scanning using the following matrix:\n ");
	for (int w = 0; w < columns; w++)
	{
		printf("%5d", w);	
		//printf("\t%d", w);	
	}
	printf("\n");
	for (int x = 0; x < rows; x++)
	{
		printf("%2d", x);
		//printf("%d\t", x);
		for (int y = 0; y < columns; y++)
		{
			if (matrix[x][y] < 0)
			{
				if (matrix[x][y] == -80)
				{
					printf("   0d");
					//printf("0d\t");
				}
				else printf("%4dd", matrix[x][y] * -1);
				//else printf("%dd\t", matrix[x][y] * -1);
							}
			else if (matrix [x][y] == 80)
				printf("   0s");
				//printf("0s\t");
			else printf("%4ds", matrix[x][y]);
			//else printf("%ds\t", matrix[x][y]);
		}
		printf("\n");
	}
}

void parseTokens(char * tok, TransitionMatrix * tm)
{
	//because of the way the strings are parsed, the extra spaces are removed. this will put
	//them back and allow the code to pass the tests accordingly.
	char token[strlen(tok) + 2];
	strncpy(token, tok, strlen(tok));
	token[strlen(tok)] = ' ';
	token[strlen(tok) + 1] = '\0';

	//this will follow the progression the string from one character to the next, going from 
	//the current state to its successor according to the transition matrix.
	int currentState = tm->startState;
	int newState, tempState;
	printf("%d ", currentState);
	for (unsigned int x = 0; x< strlen(token); x++)
	{
		newState = parseChar(token[x]);
		tempState = tm->matrix[currentState][newState];
		//takes into account the workaround for the 0 state.
		if (tempState == -80 || tempState == 80)
		{
			currentState = 0;
		}
		else currentState = tempState;
		//takes into account the delete option associated with each state.
		if (tempState < 0 )
		{	
			currentState *= -1;
		}
		printf("%d ", currentState);
		//takes into account the EOF character.
		if (newState == 10) 
		{ 
			printf("EOF\n");
			return;
		}
		//if a null transition is reached, immediately rejects the string
		if (currentState == 99)
		{
			printf("rejected\n");
			return;	
		}
	}
	//if at the end of the string it is in an accept state, accept.
	if (currentState == tm->endState) 
		printf("recognized '%s'\n", tok);
	//otherwise reject
	else 
	{
		printf("rejected\n");
	}
}

int parseChar(char token)
{
	if(token == EOF)
	{
		return CC_EOF;
	}
	else if (isdigit(token) != 0)
	{
		if (token == '0')
			return CC_DIG_0;

		else if (token == '8' || token == '9') 
			return CC_DIG_8_9;
		
		else return CC_DIG_1_7;
	}	
	else if (isalpha(token) != 0 || token == '_')
	{
		return CC_ALPHA;
	}
	else if (token == '/')
		return CC_CHAR_SLASH;
	
	else if (token == '*')
		return CC_CHAR_STAR;

	else if (token == '+' || token == '-' || token == '%')
		return CC_ARITH_OP;
	else if (isspace(token) != 0)
	{
		if(token == '\n')
			return CC_NEWLINE;
		else return CC_WS;
	}
	//characters before 0 are not generally recognized as valid characters. the extended ASCII
	//characters are from 128-255. eof is an exception, which is why it was defined earlier.
	else if(token < 0 )
		return CC_ERROR;
	else return CC_OTHER;
}  

int main(int argc, char * argv[])
{
	//requires two arguments, and will fail less than two arguments.
	if (argc < 2)
	{
		fprintf(stderr, "usage: ./tokenize tmfile\n");
		exit (1);
	}
	FILE * pFile;
	pFile = fopen (argv[1], "r");
	//if file does not open, report a system error and exit. 
	if (pFile == NULL)
	{
		perror (argv[1]);
		return 1; //Exit Failure
	}
	TransitionMatrix * tm = createTM(pFile);
	fclose(pFile);
	printMatrix(tm->rows, tm->matrix);
	char scanTok[256];
	//take input and parse it, according to the parse token method. however, the /* multiline
	//is a special edge case.
	while (scanf("%255s", scanTok) != EOF)
	{
		// edge case for multiline comments.
		//if the first two characters make up the multiline comment, initiate edge case.
		if (strncmp("/*", scanTok, 2) == 0 && !(scanTok[strlen(scanTok)] == '/' && scanTok[strlen(scanTok)-1] == '*'))
		{
			//this will take all characters, one at a time, until it reaches EOF, a */ end
			//comment, or runs out of space on the buffer
			char testChar, lastChar;
			testChar = getchar();
			lastChar = 0;			
			int len = strlen(scanTok);
			while (parseChar(testChar) < 10)
			{
				scanTok[len] = testChar;
				len++;	
				if (testChar == '/' && lastChar == '*') break;
				
				lastChar = testChar;
				testChar = getchar();
				if (len == 255) break;
			}
			scanTok[len] = '\0';
		}	
		parseTokens(scanTok, tm);
	}
	//if an EOF is reahed, run a final cehck with EOF. 
	char endString[] = {EOF, '\0'};
	
	parseTokens(endString, tm);
	free(tm);
	return 0;
}
